-- MySQL dump 10.13  Distrib 8.0.12, for Linux (x86_64)
--
-- Host: localhost    Database: bot
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='type = 0:common, 1:long, 2:short, 3: 1-sem';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (2729,'Database and Client/Server Applications (SDP 6)',0),(2732,'IT-infrastructure',0),(2733,'Computer systems architechture',0),(2734,'Human/Computer Interaction and Communication (SDP 7)',0),(2736,'Introduction to SAP ERP (ERP 1)',1),(2739,'4. Development of mobile applications for IOS (Mobile 1)',1),(2764,'Introduction to R (BDA-1)',1),(2765,'Design patterns in Java (JD-1)',1),(2766,'An Introduction to solving ACM ICPC problems (ACM-1)',1),(2767,'Programming on Python Language',3),(2768,'1С: Programming',3),(2769,'Cybersecurity',3),(2770,'Application development on MS .NET Framework platform',3),(2771,'Innovation Management',3);
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_classes`
--

DROP TABLE IF EXISTS `user_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_classes`
--

LOCK TABLES `user_classes` WRITE;
/*!40000 ALTER TABLE `user_classes` DISABLE KEYS */;
INSERT INTO `user_classes` VALUES (1,416920488,2736),(2,416920488,2768),(5,684373700,2739),(6,684373700,2768),(11,396543513,2765),(12,396543513,2769),(26,186794657,2764),(27,186794657,2769),(28,330932157,2765),(29,330932157,2767),(30,396732550,2739),(31,396732550,2767),(32,343108158,2765),(33,343108158,2767),(34,171731069,2768),(35,171731069,2765),(36,447697440,2766),(37,447697440,2767),(38,185756133,2736),(39,185756133,2767),(40,477478829,2739),(41,477478829,2771),(42,240536807,2770),(43,240536807,2736),(46,348090195,2736),(47,348090195,2771),(48,348090195,2771),(49,545209160,2767),(50,545209160,2739),(51,521150281,2736),(52,521150281,2768),(53,462924380,2736),(54,462924380,2771),(55,385020965,2767),(56,385020965,2766),(63,311260603,2739),(64,311260603,2767),(65,317190884,2770),(66,317190884,2765),(69,382341531,2736),(70,382341531,2768),(71,382341531,2768),(72,382341531,2768),(73,382341531,2768),(74,382341531,2768),(77,154904602,2739),(78,154904602,2771),(80,455560154,2768),(81,455560154,2739),(82,203721303,2765),(83,203721303,2767);
/*!40000 ALTER TABLE `user_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `group_name` tinytext,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (154904602,22173,'IS-1611 R'),(171731069,22167,'IS-1605 К'),(185756133,22170,'IS-1608 K'),(186794657,22171,'IS-1609 R'),(203721303,22171,'IS-1609 R'),(240536807,22173,'IS-1611 R'),(311260603,22166,'IS-1604 К'),(317190884,22166,'IS-1604 К'),(330932157,22168,'IS-1606 К'),(343108158,22171,'IS-1609 R'),(344721732,22171,'IS-1609 R'),(348090195,22172,'IS-1610 R'),(382341531,22166,'IS-1604 К'),(385020965,22165,'IS-1603 К'),(396543513,22171,'IS-1609 R'),(396732550,22171,'IS-1609 R'),(416920488,22171,'IS-1609 R'),(447697440,22169,'IS-1607 К'),(455560154,22171,'IS-1609 R'),(462924380,22173,'IS-1611 R'),(477478829,22168,'IS-1606 К'),(521150281,22171,'IS-1609 R'),(521749229,22171,'IS-1609 R'),(545209160,22171,'IS-1609 R'),(684373700,22171,'IS-1609 R');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-03  5:39:44
