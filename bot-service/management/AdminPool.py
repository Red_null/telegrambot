from telegram import User

from management.Admin import Admin
from management.student.StudentsPool import StudentsPool


class AdminPool:
    admin_nicknames = ['@RdNull']
    admins = []

    def __init__(self, students: StudentsPool):
        self.students = students

    def is_admin(self, user: User):
        if user.name in self.admin_nicknames:
            self.admins.append(user.id)
            return True
        return False

    def get(self, user: User):
        if self.is_admin(user):
            return Admin(user.id)
        else:
            return False

    def get_users_number(self):
        return len(self.students.users)
