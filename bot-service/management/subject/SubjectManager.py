from management import all_subjects


# todo create different classes: LongSubject, ShortSubject etc.
class SubjectManager:
    def __init__(self):
        long_subjects = {}
        short_subjects = {}
        elective_subjects = {}
        common_subjects = {}

        for key in all_subjects:
            subject = all_subjects.get(key)
            if subject.type == 0:
                common_subjects[subject.id] = subject
            elif subject.type == 1:
                long_subjects[subject.id] = subject
            elif subject.type == 2:
                short_subjects[subject.id] = subject
            elif subject.type == 3:
                elective_subjects[subject.id] = subject

        self.subjects_list = all_subjects
        self.long_subjects = long_subjects
        self.short_subjects = short_subjects
        self.elective_subjects = elective_subjects
        self.common_subjects = common_subjects

    def common_subjects_as_list(self):
        subject_list = []
        for each in self.common_subjects:
            subject_list.append(self.common_subjects.get(each))
        return subject_list

    def get(self, subject_id):
        subject = self.subjects_list.get(subject_id)
        return subject
