class Subject:
    def __init__(self, subject_id, name, subject_type):
        self.id = subject_id
        self.name = name
        self.type = subject_type

    def __str__(self):
        return '#{}/\"{}\"/{}'.format(self.id, self.name, self.type)

    def __repr__(self):
        return self.__str__()
