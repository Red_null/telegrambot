import json

import requests

import const
from database.DBConnection import DBConnection
from management.subject.Subject import Subject


def get_all_groups(course, speciality_id):  # todo add course and speciality selection
    base_group_url = 'http://schedule.iitu.kz/rest/user/get_group.php?course={}&specialty_id={}'
    groups_json_url = base_group_url.format(course, speciality_id)

    response = requests.get(groups_json_url)
    passed_info = json.loads(response.content).get('result')
    groups_list = {}
    for each in passed_info:
        groups_list[int(each.get('id'))] = each.get('name_en')
    return groups_list


all_subjects = {}
all_groups = get_all_groups(const.COURSE, const.SPECIALITY_ID)

for row in DBConnection().fetch("select id, name,type from classes"): 
    subject_type = row[2]
    subject_id = row[0]
    subject = Subject(subject_id, row[1], subject_type)
    all_subjects[subject_id] = subject
