from database.DBConnection import DBConnection as Database
from management.group.Group import Group
from management.group.GroupManager import GroupManager
from management.subject.Subject import Subject
from management.subject.SubjectManager import SubjectManager


class Student:
    electives = {}
    common_subjects = {}

    def __init__(self, user_id):
        self.id = user_id
        db_connection = Database()
        self.__db_connection__ = db_connection
        user_subjects = db_connection.fetch('select * from user_classes where user_id = {}'.format(user_id))
        subject_manager = SubjectManager()
        self.subject_manager = subject_manager
        self.common_subjects.update(subject_manager.common_subjects)
        for each in user_subjects:
            subject_id = each[2]
            subject: Subject = subject_manager.subjects_list.get(subject_id)
            self.electives[subject_id] = subject

        group_id = db_connection.fetch_one('select group_id from user_group where user_id={}'.format(user_id))
        if group_id is not None:
            group_id = group_id[0]
            self.group = GroupManager().get(group_id)
        else:
            self.group = None

    def __repr__(self):
        electives = []
        for each in self.electives:
            electives.append(each)
        common = []
        for each in self.common_subjects:
            common.append(each)
        return '#\"{}\" Group:\"{}\" CommonSubjects:\"{}\" Electives:\"{}\"'.format(self.id, self.group, common,
                                                                                    electives)

    def add_subject(self, subject: Subject):
        query = 'insert into user_classes (user_id, class_id) values ({},{})'.format(self.id, subject.id)
        self.__db_connection__.execute(query)
        subject = self.subject_manager.subjects_list.get(subject.id)
        self.electives[subject.id] = subject

    def purge_classes(self):
        query = 'delete from user_classes where user_id={}'.format(self.id)
        self.__db_connection__.execute(query)
        self.electives = {}

    def set_group(self, group: Group):
        if self.group is None:
            query = 'insert into user_group (user_id, group_id, group_name) values ({},{},\'{}\')'.format(self.id,
                                                                                                          group.id,
                                                                                                          group.name)
        else:
            query = 'update user_group set group_id={}, group_name=\'{}\' where user_id={}'.format(group.id,
                                                                                                   group.name, self.id)
        self.__db_connection__.execute(query)
        self.group = group

    def delete_group(self):
        query = 'delete from user_group where user_id={}'.format(self.id)
        self.__db_connection__.execute(query)
        self.group = None
