from management.student.Student import Student


class StudentsPool:
    users = {}

    def __repr__(self):
        return 'Users: {}'.format(self.users)

    def add_user(self, user: Student):
        self.users[user.id] = user

    def count(self):
        return len(self.users)

    def get(self, user_id):
        student = self.users.get(user_id)
        if student is None:
            student = Student(user_id)
            self.add_user(student)
        return student
