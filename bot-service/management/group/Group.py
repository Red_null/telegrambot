class Group:
    def __init__(self, group_id, name):
        self.id = group_id
        self.name = name

    def __repr__(self):
        return '#{} {}'.format(self.id, self.name)
