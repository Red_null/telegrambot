from management import all_groups
from management.group.Group import Group


class GroupManager:
    def __init__(self):
        self.groups = {}
        for each in all_groups:
            group_name = all_groups.get(each)
            self.groups[each] = Group(each, group_name)

    def get(self, group_id):
        return self.groups.get(group_id)
