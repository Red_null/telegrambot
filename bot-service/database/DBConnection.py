import os

import mysql.connector as database


class DBConnection:
    debug = False

    def __init__(self):
        try:
            if self.debug:
                password = os.environ['DB_PASSWORD']
                print(password)
                self.db = database.connect(
                    host='127.0.0.1',
                    port=33061,
                    user='bot_worker',
                    password=password,
                    database='bot'
                )
            else:
                password = os.environ['DB_PASSWORD']
                self.db = database.connect(
                    host='database',
                    port=3306,
                    user='bot_worker',
                    password=password,
                    database='bot'
                )
        except database.Error as error:
            print(error)

    def __del__(self):
        try:
            self.db.close()
        except database.Error as error:
            print(error)

    def fetch(self, query):
        try:
            self.db.connect()
            cursor = self.db.cursor()
            cursor.execute(query)
            response = cursor.fetchall()
            self.db.close()
            return response
        except database.Error as err:
            print(err)

    def fetch_one(self, query):
        try:
            self.db.connect()
            cursor = self.db.cursor()
            cursor.execute(query)
            response = cursor.fetchone()
            self.db.close()
            return response
        except database.Error as err:
            print(err)

    def execute(self, query, value=None):
        try:
            self.db.connect()
            cursor = self.db.cursor()
            cursor.execute(query, value)
            self.db.commit()
            self.db.close()
        except database.Error as err:
            print(err)
            print('query: {}'.format(query))
