import logging


def log(func):
    def wrapper(*args, **kwargs):
        logger = logging.getLogger(func.__name__)
        logger.info(f'{func.__name__}_log')
        func(*args, **kwargs)

    return wrapper
