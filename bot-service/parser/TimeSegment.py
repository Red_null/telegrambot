import datetime
import pytz
from parser import time_segments


class TimeSegment:
    timezone = 'Asia/Almaty'
    segment_time_delta = 7

    def __init__(self):

        self.time_segments: dict = time_segments

    def get_current_time(self):
        time = str(datetime.datetime.now(pytz.timezone(self.timezone)).time())
        return time[0:-7]

    def get_current_day(self):
        day = str(datetime.datetime.now(pytz.timezone(self.timezone)).weekday() + 1)
        return day

    def get_segment(self, time=None):
        if time is None:
            time = self.get_current_time()
        hour = int(time[0:2])
        minute = int(time[3:5])
        segment = hour - self.segment_time_delta

        if hour < 12 and minute >= 40:
            segment += 1
        elif 12 <= hour < 17 and minute >= 50:
            segment += 1
        elif hour > 18 and minute < 10:
            segment -= 1

        if segment > 13 or segment < 1:
            return 0
        return segment

    def get_segment_time_range(self, segment=None):
        if segment is None:
            time = self.get_current_time()
            segment = str(self.get_segment(time))

        return self.time_segments.get(segment)
