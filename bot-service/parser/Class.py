class Class:
    lang = 'en'

    def __init__(self, class_dict: dict):
        self.id = int(class_dict.get('id'))
        self.subject = class_dict.get('subject')
        self.subject_type = class_dict.get('subject_type')
        self.teacher = class_dict.get('teacher')
        self.room = class_dict.get('room')

    def __str__(self):
        return 'Subject: {}\nType: {}\nTeacher: {}\nRoom: {}'.format(self.subject, self.subject_type, self.teacher,
                                                                     self.room)
