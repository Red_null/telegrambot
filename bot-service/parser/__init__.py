import json
import os

with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'files/timeSegments.json')) as file:
    time_segments = json.load(file)
