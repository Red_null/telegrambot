import requests
import json
import parser.TimeSegment as Segment
import parser.Class as Class


class Parser:
    lang = 'en'

    def __init__(self, group_id=47703, debug=False):
        self.debug = debug

        full_json = self.load(group_id)
        self.time_table: dict = full_json.get('timetable')
        self.subjects: dict = full_json.get('subjects')
        self.subject_types: dict = full_json.get('subject_types')
        self.teachers: dict = full_json.get('teachers')
        self.rooms: dict = full_json.get('bundles')

    def prettify_json(self, raw: dict):
        return json.dumps(raw, indent=4, sort_keys=True)

    def load(self, group_id):
        if self.debug:
            passed_info = json.loads(open('files/get_timetable_block.php.json', 'r').read())
        else:
            schedule_json_url = f'http://schedule.iitu.kz/rest/user/get_timetable_block.php?block_id={group_id}'  # noqa
            table = requests.get(schedule_json_url)
            passed_info = json.loads(table.content)

        time_table: dict = passed_info
        return time_table

    def get_next_classes(self, day=None, time_segment=None):
        segment = Segment.TimeSegment()
        if day is None:
            day = segment.get_current_day()

        if time_segment is None:
            time_segment = segment.get_segment()

        day_table: dict = self.time_table.get(str(day))
        if not day_table:
            return False

        classes = day_table.get(str(time_segment))
        classes_list = []
        if classes is not None:
            for each in classes:
                classes_list.append(self.parse_class(each))
            return classes_list
        else:
            return False

    def parse_class(self, pure_class_info: dict):
        class_info = dict()
        subject_name = self.subjects.get(pure_class_info.get('subject_id')).get('subject_{}'.format(self.lang))
        class_info['subject'] = subject_name

        class_id = pure_class_info.get('subject_id')
        class_info['id'] = class_id

        subject_type = self.subject_types.get(pure_class_info.get('subject_type_id')).get(
            'subject_type_{}'.format(self.lang))
        class_info['subject_type'] = subject_type

        teacher = self.teachers.get(pure_class_info.get('teacher_id')).get('teacher_{}'.format(self.lang))
        class_info['teacher'] = teacher

        room = self.rooms.get(pure_class_info.get('bundle_id')).get('0').get('name_en')
        class_info['room'] = room
        return Class.Class(class_info)
