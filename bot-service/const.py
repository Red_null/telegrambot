import logging

COURSE = 4
SPECIALITY_ID = 6992

LOG_FILE = 'logs.txt'

LOGGER_CONF = {
    'level': logging.INFO,
    'format': '%(asctime)s %(levelname)-8s %(name)-15s |  %(message)s',
    'datefmt': '%d-%m %H:%M:%S',
}

LOGGER_PROD_CONF = {
    **LOGGER_CONF,
    'filemode': 'a',
    'filename': LOG_FILE
}
