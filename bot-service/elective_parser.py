"""
Use it only when subjects changes todo update it
"""
import mysql.connector as database
from mysql.connector import errorcode

from database.DBConnection import DBConnection
from parser.ScheduleParser import Parser


def get_all_subjects():
    parser = Parser(143009)
    subjects: dict = parser.subjects
    try:
        db = DBConnection()
        query = 'insert into classes(id, name, type) values (%s,%s,%s)'
        print('Subjects:')
        for idx, subject in subjects.items():
            values = (idx, subject['subject_en'], 0)
            print(values)
            try:
                db.execute(query, values)
            except Exception as exc:
                print(exc)

    except database.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
        raise


if __name__ == '__main__':
    print('ye')
    get_all_subjects()
