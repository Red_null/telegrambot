import datetime
import logging
import os

from telegram import Update, User, Message, ReplyKeyboardMarkup, \
    KeyboardButton, InlineKeyboardButton, InlineKeyboardMarkup, CallbackQuery
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

import const
from management.Admin import Admin
from management.AdminPool import AdminPool
from management.group.GroupManager import GroupManager
from management.student.StudentsPool import StudentsPool
from management.subject.SubjectManager import SubjectManager
from parser.ScheduleParser import Parser as ScheduleParser
from parser.TimeSegment import TimeSegment as Segment
from utils import log

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

PRODUCTION = os.environ.get('PRODUCTION')

boot_time = datetime.datetime.now()
token = os.environ['TOKEN']

students_pool = StudentsPool()
admin_pool = AdminPool(students_pool)
subject_manager = SubjectManager()
group_manager = GroupManager()


def main():
    LOGGER.info(f'Bot started')
    updater = Updater(token)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('time', utc_time))
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', info))
    dp.add_handler(CommandHandler('upt', get_uptime))

    dp.add_handler(CommandHandler('t', get_neighbour_segment))
    dp.add_handler(CommandHandler('n', next_class))

    dp.add_handler(CommandHandler('setup', update_classes))
    dp.add_handler(CommandHandler('purge_electives', purge_classes))

    dp.add_handler(CallbackQueryHandler(query_handler))

    dp.add_handler(CommandHandler('info', get_user_info))
    dp.add_handler(CommandHandler('admin', admin, pass_args=True))

    # Start the Bot
    updater.start_polling()
    # Run bot
    LOGGER.info('Load completed')
    updater.idle()


@log
def query_handler(bot, update: Update):
    query: CallbackQuery = update.callback_query
    query_data: str = query.data
    user_id = query.from_user.id
    student = students_pool.get(user_id)

    text = 'Not supported action'
    elective_setup_tag = True if query_data.find('@setup_subject') == 0 else False
    group_setup_tag = True if query_data.find('@setup_group') == 0 else False

    if elective_setup_tag:
        subject_id = int(query_data[query_data.find(':') + 1:])
        subject = subject_manager.get(subject_id)
        student.add_subject(subject)
        text = 'Selected subject:\n{}'.format(subject.name)

    if group_setup_tag:
        group_id = int(query_data[query_data.find(':') + 1:])
        group = group_manager.get(group_id)
        student.set_group(group)
        text = "Group selected:\n{}".format(group.name)

    update_on_callback(bot, query, text)

    if group_setup_tag:
        user_id = query.from_user.id
        update_classes(bot, update, user_id)


def update_on_callback(bot, query, text='Selected option: '):
    bot.edit_message_text(text="{}".format(text),
                          chat_id=query.message.chat_id,
                          message_id=query.message.message_id)


@log
def utc_time(bot, update):
    update.message.reply_text('UTC Time: {}'.format(datetime.datetime.now()))


@log
def start(bot, update):
    info(bot, update)
    message: Message = update.message
    user: User = message.from_user
    student = students_pool.get(user.id)
    custom_keyboard = [[KeyboardButton('/t'), '/n']]
    reply_markup = ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    update.message.reply_text('Shortcuts buttons:', reply_markup=reply_markup)
    set_group(bot, update)


@log
def info(bot, update):
    update.message.reply_text('Created by: Luboshnikov Anton\n'
                              'Available commands:\n'
                              '/n - next subjects with time\n'
                              '/t - current and next class time range\n'
                              '/setup - set electives\n'
                              '/purge_electives - remove all selected classes\n'
                              '(will show all classes after it)\n'
                              '/info - shows your electives and group\n'
                              )

@log
def get_uptime(bot, update):
    update.message.reply_text('Uptime: {}'.format(datetime.datetime.now() - boot_time))


@log
def get_neighbour_segment(bot, update):
    message: Message = update.message
    segment = Segment()

    current_segment = segment.get_segment()
    current_time: dict = segment.get_segment_time_range()
    if current_segment != 0:
        message.reply_text('Now: {} - {}'.format(current_time.get('start_time'), current_time.get('end_time')))
    else:
        message.reply_text('Current time out of range')
    if current_segment + 1 != 0:
        next_time: dict = segment.get_segment_time_range(str(current_segment + 1))
        message.reply_text('Next: {} - {}'.format(next_time.get('start_time'), next_time.get('end_time')))
    else:
        message.reply_text('Next time out of range')


@log
def get_user_info(bot, update):
    message: Message = update.message
    user: User = message.from_user
    student = students_pool.get(user.id)
    group = student.group
    if group is None:
        message.reply_text('Type /start to set up')
        return

    message.reply_text('Group: {}'.format(group.name))

    subscribed_subjects = student.electives
    if len(subscribed_subjects) == 0:
        message.reply_text('You are not subscribed\nType /setup')

    reply = ''

    for each in subscribed_subjects:
        subject = subject_manager.get(each)
        reply += '- {}\n'.format(subject.name)
    if len(reply) > 0:
        message.reply_text('Subscribed classes:\n{}'.format(reply))


@log
def next_class(bot, update):
    message: Message = update.message
    user: User = message.from_user

    student = students_pool.get(user.id)
    if student.group:
        timetable_parser = ScheduleParser(student.group.id)
    else:
        message.reply_text('Type /start to set up')
        return
    classes: list = timetable_parser.get_next_classes()  # list of Class
    get_neighbour_segment(bot, update)

    if classes is not False:
        class_flag = False
        for each in classes:
            if each.id in student.electives or each.id in student.common_subjects:
                class_flag = True
                message.reply_text(str(each))
        if not class_flag:
            message.reply_text('Nope')
    else:
        message.reply_text('Nope')


@log
def set_course(bot, update):
    message: Message = update.message


@log
def set_speciality(bot, update):
    message: Message = update.message


@log
def set_group(bot, update):
    message: Message = update.message
    keyboard = fill_group_keyboard(group_manager.groups)
    message.reply_text('Choose group:', reply_markup=keyboard)


@log
def setup_classes(bot, update, message=None):
    if message is None:
        message: Message = update.message
    long_track_list = subject_manager.long_subjects
    short_track_list = subject_manager.short_subjects
    electives_list = subject_manager.elective_subjects

    if len(long_track_list) > 0:
        message.reply_text('Choose long track:', reply_markup=fill_subject_keyboard(long_track_list))
    if len(short_track_list) > 0:
        message.reply_text('Choose short track:', reply_markup=fill_subject_keyboard(short_track_list))
    if len(electives_list) > 0:
        message.reply_text('Choose current 1-semester:', reply_markup=fill_subject_keyboard(electives_list))


def fill_subject_keyboard(subjects_list):
    keyboard = []

    for subject_id in subjects_list:
        data = '@setup_subject:{}'.format(subject_id)
        keyboard.append([
            InlineKeyboardButton(str(subjects_list.get(subject_id).name), callback_data=data)
        ])
    return InlineKeyboardMarkup(keyboard)


def fill_group_keyboard(group_list):
    keyboard = []
    keyboard_line = []
    counter = 0

    for group_id in group_list:
        group = group_list.get(group_id)
        data = '@setup_group:{}'.format(group.id)
        keyboard_line.append(
            InlineKeyboardButton(group.name, callback_data=data)
        )
        if counter % 3 == 2:
            keyboard.append(keyboard_line)
            keyboard_line = []
        counter += 1
    return InlineKeyboardMarkup(keyboard)


def fill_speciality_keyboard(speciality_list):
    pass


def update_classes(bot, update: Update, user_id=None):
    message: Message = update.message

    if user_id is not None:
        bot.send_message(chat_id=user_id, text='Updating electives')
        student = students_pool.get(user_id)
        student.purge_classes()
        setup_classes(bot, update, update.callback_query.message)
    else:
        message.reply_text('Updating electives')
        student = students_pool.get(message.from_user.id)
        student.purge_classes()
        setup_classes(bot, update)


@log
def purge_classes(bot, update):
    message: Message = update.message
    user: User = message.from_user
    student = students_pool.get(user.id)
    student.purge_classes()
    message.reply_text('All your data removed')


@log
def admin(bot, update, args):
    message: Message = update.message
    user: User = message.from_user

    admin: Admin = admin_pool.get(user)

    if admin is False:
        return

    if 'usr' in args:
        message.reply_text('Users since uptime: {}'.format(admin_pool.get_users_number()))

    if 'log' in args:
        try:
            message.reply_document(document=open('logs.txt', 'rb'))
        except Exception as exc:
            message.reply_text(str(exc))


if __name__ == '__main__':
    if PRODUCTION:
        logging.basicConfig(**const.LOGGER_PROD_CONF)
    else:
        logging.basicConfig(**const.LOGGER_CONF)
    main()

"""
def template(bot, update):
    message: Message = update.message
    user: User = message.from_user
"""
